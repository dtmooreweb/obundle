import PageManager from './page-manager';

export default class Custom extends PageManager {

    onReady() {
        function addMultiItems() {
            fetch('/api/storefront/cart', {
                credentials: 'include',
            }).then(function(response) {
                return response.json();
            }).then(function(myJson) {
                if(myJson.length > 0) {
  	                var cartID = myJson[0]['id'];
                    postData(`/api/storefront/carts/`, cartID ,{
                        "lineItems": [
                            {
                                "quantity": 1,
                                "productId": 111
                            },
                            {
                                "quantity": 1,
                                "productId": 107
                            },
                            {
                                "quantity": 1,
                                "productId": 104
                            }
                            ]}
                        )
                .then(data => console.log(JSON.stringify(data))) 
                .catch(error => console.error(error));
                function postData(url = ``, cartId = ``, cartItems = {}) {
                return fetch(url + cartId + '/items', {
                method: "POST",
                credentials: "same-origin",
                headers: {
                    "Content-Type": "application/json" },
                body: JSON.stringify(cartItems), 
            })
                .then(response => response.json()); 
            }
        } else {
        postData(`/api/storefront/cart`, {
            "lineItems": [
            {
                "quantity": 1,
                "productId": 111
            },
            {
                "quantity": 1,
                "productId": 107
        },
        {
            "quantity": 1,
            "productId": 104
            }
            ]}
        )
      .then(data => console.log(JSON.stringify(data))) 
      .catch(error => console.error(error));

                function postData(url = ``, cartItems = {}) {
              return fetch(url, {
              method: "POST",
              credentials: "same-origin",
              headers: {
                  "Content-Type": "application/json" },
                          body: JSON.stringify(cartItems), 
                    })
                        .then(response => response.json()); 
                    }
                }
  
  
            });
        }
        function addMultiItemsOptions() {
            fetch('/api/storefront/cart', {
                credentials: 'include'
            }).then(function(response) {
                return response.json();
            }).then(function(myJson) {
                if(myJson.length > 0) {
  	                var cartID = myJson[0]['id'];
                    postData(`/api/storefront/carts/`, cartID ,{
                        "lineItems": [
                        {
                            "quantity": 1,
                            "productId": 93,
                            "variantId": 46
                        },
                        {
                            "quantity": 1,
                            "productId": 93,
                            "variantId": 51
                        },
                        {
                            "quantity": 1,
                            "productId": 93,
                            "variantId": 56
                        }
                        ]}
                        )
              .then(data => console.log(JSON.stringify(data))) 
              .catch(error => console.error(error));
              function postData(url = ``, cartId = ``, cartItems = {}) {
              return fetch(url + cartId + '/items', {
              method: "POST",
              credentials: "same-origin",
              headers: {
                  "Content-Type": "application/json" },
              body: JSON.stringify(cartItems), 
          })
          .then(response => response.json()); 
        }
        } else {
        postData(`/api/storefront/cart`, {
            "lineItems": [
            {
                "quantity": 1,
                "productId": 93,
                "variantId": 46
            },
            {
                "quantity": 1,
                "productId": 93,
                "variantId": 51
            },
            {
                "quantity": 1,
                "productId": 93,
                "variantId": 56
            }
            ]}
        )
      .then(data => console.log(JSON.stringify(data))) 
      .catch(error => console.error(error));

                function postData(url = ``, cartItems = {}) {
              return fetch(url, {
              method: "POST",
              credentials: "same-origin",
              headers: {
                  "Content-Type": "application/json" },
                          body: JSON.stringify(cartItems), 
                    })
                        .then(response => response.json()); 
                    }
                }
  
  
            });
        }

        document.getElementById('addMultiple').onclick = function () {
            addMultiItems();
        };
        document.getElementById('addMultipleOptions').onclick = function () { 
        	addMultiItemsOptions();
        };
    }
}

